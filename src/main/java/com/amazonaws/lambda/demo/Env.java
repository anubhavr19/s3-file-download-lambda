package com.amazonaws.lambda.demo;

import java.util.Objects;
import java.util.stream.Stream;

public class Env {
	public static String AWS_REGION() {
		return locateValue("BUCKET_REGION");
	}

	public static String bucketName() {
		return locateValue("BUCKET_NAME");
	}

	private static String locateValue(String key) {
		return Stream.of(System.getenv(key), System.getProperty(key)).filter(Objects::nonNull).findAny().orElse(null);
	}

}